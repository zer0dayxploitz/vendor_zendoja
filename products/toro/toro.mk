# Inherit AOSP device configuration for toro.
$(call inherit-product, device/samsung/toro/full_toro.mk)

# Tell compiler to build Superuser.apk
PRODUCT_PACKAGES += \
        Superuser \
	su



# Tuna overlay
PRODUCT_PACKAGE_OVERLAYS += device/samsung/tuna/overlay
PRODUCT_PACKAGE_OVERLAYS += device/samsung/toro/overlay
PRODUCT_PACKAGE_OVERLAYS += vendor/zendoja/overlay/toro

# Setup device specific product configuration.
PRODUCT_NAME := zendoja_toro
PRODUCT_BRAND := Google
PRODUCT_DEVICE := toro
PRODUCT_MODEL := Galaxy Nexus
PRODUCT_MANUFACTURER := Samsung

PRODUCT_BUILD_PROP_OVERRIDES := PRODUCT_NAME=mysid BUILD_ID=ICL53F BUILD_FINGERPRINT=google/mysid/toro:4.0.2/ICL53F/235179:user/release-keys PRIVATE_BUILD_DESC="mysid-user 4.0.2 ICL53F 235179 release-keys" BUILD_NUMBER=235179

# Copy toro specific prebuilt files
PRODUCT_COPY_FILES +=  \
    vendor/zendoja/prebuilt/xhdpi/bootanimation.zip:system/media/bootanimation.zip
